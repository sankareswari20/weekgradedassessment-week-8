package com.hcl.bean;

import org.springframework.stereotype.Component;

@Component
public class Book {
	private int Id;
	private String title;
	private String genre;
	
	public Bookees() {
		super();
	}
//paraeterized constractor
	public Bookees(int Book_Id, String  Book_title, String  Book_genre) {
		super();
		this.Id =  Book_Id;
		this.title =  Book_title;
		this.genre =  Book_genre;
	}
  // using setter method
	public void setBookId(int bookId) {
		this.Id = bookId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}
	
    //using getter method
	public int getBookId() {
		return Id;
	}
	public String getTitle() {
		return title;
		
		@Override
		public String toString() {
			return "Books [book_id=" + Book_Id + ", book_title=" +Book_title + ", book_genre=" + Book_genre +  "]";
		}


}
