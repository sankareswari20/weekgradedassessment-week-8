package com.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Books;

@Repository

public class BookDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	public List<Books> getAllBooks(){
		try {
			return jdbcTemplate.query("select * from books",new bookRowMapper());
		} catch (Exception e) {
			System.out.println("in retriving books"+e);
			return null;
		}
		
		
	}
	public int storeLikedBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into likedbooks values(?,?,?,?)",books.getBid(),books.getTitle(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in liked books"+e);
			return 0;
		}
	}
	public int storeReadLaterBooks(Books books,String user) {
		try {
			return jdbcTemplate.update("insert into readlaterbooks values(?,?,?,?)",books.getBid(),books.getTitle(),
					books.getGenre(),books.getImageurl(),user);
		} catch (Exception e) {
			System.out.println("in Readlater books"+e);
			return 0;
		}
	}
	public List<Books> getAllLikedBooks(String user){
		try {
			return jdbcTemplate.query("select bid,title,genre  from likedbooks ",new likedBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
	public List<Books> getAllReadLaterBooks(String user){
		try {
			return jdbcTemplate.query("select bid,title,genre, from readlaterbooks ",new readLaterBookRowMapper(),user);
		} catch (Exception e) {
			System.out.println("in getlikedBooks"+e);
			return null;
		}

}
}
class bookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultset, int rowNum) throws SQLException {
		Books book=new Books();
		book.setBid(resultset.getInt(1));
		book.setTitle(resultset.getString(2));
		book.setGenre(resultset.getString(3));
		book.setImageurl(resultset.getString(4));
		return book;
	}
	
}
class likedBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultset, int rowNum) throws SQLException {
		Books likebook=new Books();
		likebook.setBid(resultset.getInt(1));
		likebook.setTitle(resultset.getString(2));
		likebook.setGenre(resultset.getString(3));
		likebook.setImageurl(resultset.getString(4));
		return lb;
	}
	
}
class readLaterBookRowMapper implements RowMapper<Books>{

	@Override
	public Books mapRow(ResultSet resultset, int rowNum) throws SQLException {
		Books readlaterbook=new Books();
		 readlaterbook.setBid(resultset.getInt(1));
		 readlaterbook.setTitle(resultset.getString(2));
		 readlaterbook.setGenre(resultset.getString(3));
		 readlaterbook.setImageurl(resultset.getString(4));
		return book;
	}
	


}
