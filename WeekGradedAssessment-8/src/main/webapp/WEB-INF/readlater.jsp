<%@page import="com.hcl.bean.Books"%>
<%@page import="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ReadLater Books</title>
</head>
<h4 align="center">ReadLater BookList </h4>
<a href="Welcome.jsp">Home Page</a>
<body style="background-color:azure ;">
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>BOOK_TITLE</th>
			<th>BOOK_GENRE</th>
			
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Book> listOfBooks = (List<Book>)obj;
	Iterator<Book> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Book book  = ii.next();
		%>
		<tr>
			<td><%=book.getBid() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>
