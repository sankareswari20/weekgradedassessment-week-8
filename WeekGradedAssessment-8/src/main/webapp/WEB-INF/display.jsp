<%@page import="com.hcl.bean.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>DisplayBook</title>
</head>
<body style="background-color: azure;">
<h3 align="center"> List of Books Available </h3>
<a href="index.jsp">GO BACK</a>
<a href="welcome.spring">Login</a><br>
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>BOOKTITLE</th>
			<th>BOOK_GENRE</th>
			
	</tr>
<% 
	Object obj = session.getAttribute("obj");
	List<Book> listOfBooks = (List<Book>)obj;
	Iterator<Book> ii = listOfBooks.iterator();
	while(ii.hasNext()){
		Book book  = ii.next();
		%>
		<tr>
			<td><%=book.getBid() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>
